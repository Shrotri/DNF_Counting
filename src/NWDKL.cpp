/*
 * This file is part of Approximate DNF-Counting Suite.
 * Copyright (c) 2018, Kuldeep S. Meel, Aditya A. Shrotri, Moshe Y. Vardi
 *   
    Approximate DNF-Counting Suite is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Approximate DNF-Counting Suite is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Approximate DNF-Counting Suite.  If not, see <https://www.gnu.org/licenses/>.
    */
    
#include <vector>

#include "NWDKL.h"
#include "Timers.h"

using std::cout;
using std::endl;

double_t NWDKL::generateSample(){
	//G->printGraph();
	Network G_sample = Network(*G);
	cout<<"created copy"<<endl;
	G_sample.printGraph();
	G_sample.failEdges(rb);
	cout<<"failed edges"<<endl;
	exit(1);
	/*
	if(G_sample.areConnected(source,target)){
		cout<<"Sample generated"<<endl;
		return 0;
	}
	else{
		cout<<"Sample generated"<<endl;
		return 1;
	}
	*/
}


int NWDKL::solve(){
	double startTime = cpuTimeTotal();
	cout<<"starting counting"<<endl;
	AA();
	cout<<"Probability of failure is "<<count.num/count.den<<endl;
	cout<<"finished counting"<<endl;
	double elapsedTime = cpuTimeTotal() - startTime;
	cout<<"NWDKL took "<<elapsedTime<<" seconds"<<endl;
	return 0;
}
int main(int argc, char* argv[]){
	cout<< "Starting NWDKL at ";
	print_wall_time();
	cout<<endl;
	
	if (!(argc==6)){
		cout<<"Usage: NWDKL <eps> <delta> <s> <t> <input_dnf_dimacs_file>"<<endl;
		exit(1);
	}
	double_t eps = strtod(argv[1],NULL);
	double_t delta = strtod(argv[2],NULL);
	uint64_t source = stoi(argv[3],NULL);
	uint64_t target = stoi(argv[4],NULL);
	std::string nwFile = std::string(argv[5]);
	
	cout<<"nwDKL invoked with epsilon="<<eps<<" delta="<<delta<<" source="<<source<<" target="<<target<<" nwFile="<<nwFile<<endl<<endl;
	Network G(nwFile);
	NWDKL D(&G,eps,delta,source,target);
	int retcode = D.solve();
	
	cout<<endl<<endl<< "NWDKL ended at ";
	print_wall_time();
	cout<<endl;
	
	return retcode;
}	
