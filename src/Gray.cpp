/*
 * This file is part of Approximate DNF-Counting Suite.
 * Copyright (c) 2018, Kuldeep S. Meel, Aditya A. Shrotri, Moshe Y. Vardi
 *   
    Approximate DNF-Counting Suite is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Approximate DNF-Counting Suite is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Approximate DNF-Counting Suite.  If not, see <https://www.gnu.org/licenses/>.
    */
/*
 * Based on algorithm H in "Art of Computer Programming" by Don Knuth
 */
 
//#include <stdlib>
#include "../include/Gray.h"

using std::endl;
using std::cout;
uint32_t Gray::getNextPosition(){
	uint32_t j = f[0];
	f[0] = 0;
	if(j == numBits){
		return (-1);
	}
	f[j] = f[j+1];
	f[j+1] = j+1;
	a[j] = !a[j];
	return (j);
}

/*int main(){
	int n = 6;
	Gray g = Gray(n);
	vector<bool> V = vector<bool>(n);
	while(true){
		for(int i=0;i<n;i++){
			cout<<V[i]<<" ";
		}
		cout<<endl;
		uint32_t pos = g.getNextPosition();
		if (pos == -1){
			break;
		}
		V[pos] = !V[pos];
	}
}*/
