/*
 * This file is part of Approximate DNF-Counting Suite.
 * Copyright (c) 2018, Kuldeep S. Meel, Aditya A. Shrotri, Moshe Y. Vardi
 * Copyright (C) 2005 Linas Vepstas
 *  
    Approximate DNF-Counting Suite is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Approximate DNF-Counting Suite is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Approximate DNF-Counting Suite.  If not, see <https://www.gnu.org/licenses/>.
    */
/*
 * See https://github.com/linas/anant for the original implementation of the log function (adapted here) and many others.
 */
     
#include <iostream>
#include <vector>
#include <iomanip>
#include <algorithm>

#include "../include/KLSampler.h"

using std::cout;
using std::endl;

bool mpz_class_cmp(mpz_class a, mpz_class b){ return a<=b;} //not a<b since clauses are numbered 1 to m inclusive. lower_bound would return clause number 0 if not for a<=b

vector<bool> KLSampler::randomAssignment(){
	if (uniform){
		uint32_t logm = (uint32_t)ceil(log2(F.m));
		uint64_t clsnum = 0;
		do{
			clsnum = 0;
			string clsStr = rb.GenerateRandomBits(logm);
			for (int j=0;j<logm;j++){
				if(clsStr[j]=='1'){
					clsnum += 1<<(j);
				}
			}
			clsnum++;
		} while(clsnum>F.m);
		vector<bool> result(F.n);
		string asnmtStr = rb.GenerateRandomBits(F.n);
		for (int i=0;i<F.n;i++){
			result[i] = (asnmtStr[i]=='1');
		}
		for(int j=0; j< F.clauses[clsnum].size();j++){
			result[abs(F.clauses[clsnum][j])-1]=(F.clauses[clsnum][j]>0);
		}
			
		
		randClsNum = clsnum;
		return(result);
	}
	else{
		//cout<<"1"<<endl;
		/*Generate a uniform random integer in the range 0 to U-1, inclusive. 
		 */
		mpz_urandomm(randNum_clause,state1, U);
		//cout<<"2"<<endl;
		mpz_urandomb(randNum_assnmnt,state2, F.n);
		//cout<<"3"<<endl;
		mpz_class x(randNum_clause);
		//cout<<"Finding lower bound"<<endl;
		
		/* lower_bound
		 * Returns an iterator pointing to the first element in the range [first, last) 
		 * that is not less than (i.e. greater or equal to) value, or last if no such element is found.
		 */ 
		// if x was 0 then pos would have been samp_array.begin() making low = 0. But clauses are numbered 1 to m.
		// instead of making x = x+1, we change definition of compare function to make a < b+1 return true which is equivalent to a<=b true.
		vector<mpz_class>::iterator pos = lower_bound(sampling_array.begin(),sampling_array.end(),x, mpz_class_cmp);
		//cout<<"Found lower bound"<<endl;
		int low = pos - sampling_array.begin();
		//randCls = low;
		vector<bool> result(F.n);
		for (int i=0;i<F.n;i++){
			result[i] = mpz_tstbit(randNum_assnmnt,i);
		}	
		for(int j=0; j< F.clauses[low].size();j++){
			result[abs(F.clauses[low][j])-1]=(F.clauses[low][j]>0);
		}
		randClsNum = low;
		return(result);
	}
}

void KLSampler::calculateFraction(double_t num, uint64_t den){
	if(uniform){
		double_t logm = log2(F.m);
		double_t lognum = log2(num);
		double_t logden = log2(den);
		cout<<"Final count is 2^"<<std::setprecision(20)<<(lognum+logm+F.n-F.minClauseSize-logden)<<endl;
	}
	else{
		mpf_t mylog, fU;
		int nBits, decimal_prec;
		decimal_prec = 15;
		nBits = 3.3* decimal_prec;
		nBits += nBits/2.0; //padding
		mpf_set_default_prec(nBits);
		
		mpf_init(mylog);
		mpf_init(fU);
		mpf_set_z(fU,U);
		
		fp_log(mylog,fU,decimal_prec);
		mpf_t ln2;
		mpf_init(ln2);
		fp_log2(ln2,decimal_prec);
		mpf_div(mylog,mylog,ln2);
		gmp_printf("U is 2^%.*Ff",decimal_prec, mylog);
		cout<<endl;
		mpf_t lognum, logden;
		mpf_init(lognum);
		mpf_init(logden);
		mpf_set_d(lognum,log2(num));
		mpf_set_d(logden,log2(den));
		mpf_add(mylog,mylog,lognum);
		mpf_sub(mylog,mylog,logden);
		gmp_printf("Final count is 2^%.*Ff\n",decimal_prec, mylog);
		
		mpz_clear(U);
		mpf_clear(fU);
		mpf_clear(mylog);
		mpf_clear(lognum);
		mpf_clear(logden);
	}
}

double_t KLSampler::getLogU(){
	mpf_t mylog, fU;
	int nBits, decimal_prec;
	decimal_prec = 15;
	nBits = 3.3* decimal_prec;
	nBits += nBits/2.0; //padding
	mpf_set_default_prec(nBits);
	
	mpf_init(mylog);
	mpf_init(fU);
	mpf_set_z(fU,U);
	
	fp_log(mylog,fU,decimal_prec);
	mpf_t ln2;
	mpf_init(ln2);
	fp_log2(ln2,decimal_prec);
	mpf_div(mylog,mylog,ln2);
	double_t result = mpf_get_d(mylog);
	mpf_clear(mylog);
	mpf_clear(fU);
	mpf_clear(ln2);
	logU = result;
	return result;
}

uint32_t KLSampler::getClauseFromBits(vector<bool> clsBitVec){
	//mpz_set_ui(clsNum,0);
	//mpz_set_ui(tempClsNum,0);     // set to 0 instead of reallocating to retain memory already allocated
	mpz_init(clsNum);
	mpz_init(tempClsNum);
	for (uint32_t j = 0; j< clsBitVec.size();j++){
		if(clsBitVec[j]){
			//cout<<"1";
			mpz_ui_pow_ui(tempClsNum,2,j);
			mpz_add(clsNum,tempClsNum,clsNum);
		}
		else{
			//cout<<"0";
		}
	}
	//cout<<endl;
	//gmp_printf("clsNum: %Zd\n",clsNum);
	mpz_class x(clsNum);
	uint32_t clsNumber = -1;
	vector<mpz_class>::iterator pos = lower_bound(sampling_array.begin(),sampling_array.end(),x, mpz_class_cmp);
	//cout<<"Found lower bound"<<endl;
	clsNumber = pos - sampling_array.begin();
	mpz_clear(clsNum);
	mpz_clear(tempClsNum);   // do not deallocate these vars since they will be reused multiple times.

	return clsNumber;      // case when clsNumber > m will be handled by calling function
}

// following taken from https://github.com/linas/anant/tree/master/src
// int and unsigned int changed to uint32_t

void KLSampler::fp_epsilon (mpf_t eps, uint32_t prec)
{
	static int cache_prec = -1;
	static mpf_t cache_eps;

	if (-1 == cache_prec)
	{
		mpf_init (cache_eps);
	}

	if (prec == cache_prec)
	{
		mpf_set (eps, cache_eps);
		return;
	}
	if (cache_prec < prec)
	{
		mpf_set_prec (cache_eps, 3.322*prec+50);
	}

	/* double mex = ((double) prec) * log (10.0) / log(2.0); */
	double mex = ((double) prec) * 3.321928095;
	unsigned int imax = (unsigned int) (mex +1.0);
	mpf_t one;
	mpf_init (one);
	mpf_set_ui (one, 1);
	mpf_div_2exp (cache_eps, one, imax);

	mpf_set (eps, cache_eps);
	cache_prec = prec;
	mpf_clear (one);
}

void KLSampler::fp_log_m1 (mpf_t lg, const mpf_t z, uint32_t prec)
{
	mp_bitcnt_t bits = ((double_t) prec) * 3.322 + 50;
	mpf_t zee, z_n, term;

	mpf_init2 (zee, bits);
	mpf_init2 (z_n, bits);
	mpf_init2 (term, bits);

	/* Make copy of argument now! */
	mpf_set (zee, z);
	mpf_mul (z_n, zee, zee);
	mpf_set (lg, zee);

	/* Use 10^{-prec} for smallest term in sum */
	mpf_t maxterm;
	mpf_init2 (maxterm, bits);
	fp_epsilon (maxterm, prec);

	uint32_t n=2;
	while(1)
	{
		mpf_div_ui (term, z_n, n);
		mpf_add (lg, lg, term);

		/* don't go no farther than this */
		mpf_abs (term, term);
		if (mpf_cmp (term, maxterm) < 0) break;

		n ++;
		mpf_mul (z_n, z_n, zee);
	}

	mpf_clear (zee);
	mpf_clear (z_n);
	mpf_clear (term);

	mpf_clear (maxterm);
}

void KLSampler::fp_log2 (mpf_t l2, uint32_t prec)
{
	// lines not commented since static required since mutually recursive with fp_log
	static uint32_t precision=0;
	static mpf_t cached_log2;

	//pthread_spin_lock(&mp_const_lock);
	if (precision >= prec)
	{
		mpf_set (l2, cached_log2);
		//pthread_spin_unlock(&mp_const_lock);
		return;
	}

	if (0 == precision)
	{
		mpf_init (cached_log2);
	}
	mp_bitcnt_t bits = ((double_t) prec) * 3.322 + 50;
	mpf_set_prec (cached_log2, bits);

	mpf_t two;
	mpf_init2 (two, bits);
	mpf_set_ui (two, 2);
	fp_log (cached_log2, two, prec);
	mpf_set (l2, cached_log2);

	mpf_clear (two);
	precision = prec;
	//pthread_spin_unlock(&mp_const_lock);
}

void KLSampler::fp_log (mpf_t lg, const mpf_t z, uint32_t prec)
{
	mp_bitcnt_t bits = ((double_t) prec) * 3.322 + 50;
	mpf_t zee;
	mpf_init2 (zee, bits);
	mpf_set (zee, z);
	int32_t nexp = 0;

	/* Assume branch cut in the usual location, viz
	 * along negative z axis */
	if (mpf_cmp_ui(zee, 0) <= 0)
	{
		fprintf(stderr, "Error: bad domain for fp_log: log(%g)\n",
			mpf_get_d(zee));
		mpf_clear (zee);
		exit (1);
	}

	/* Find power of two by means of bit-shifts */
	while (mpf_cmp_ui(zee, 2) > 0)
	{
		nexp ++;
		mpf_div_ui (zee, zee, 2);
	}

	while (mpf_cmp_ui(zee, 1) < 0)
	{
		nexp --;
		mpf_mul_ui (zee, zee, 2);
	}

	/* Apply simple-minded series summation
	 * This appears to be faster than the Feynman algorithm
	 * for the types of numbers and precisions I'm encountering. */
	if (mpf_cmp_d(zee, 1.618) > 0)
	{
		mpf_ui_div (zee, 1, zee);
		mpf_ui_sub (zee, 1, zee);
		fp_log_m1 (lg, zee, prec);
	}
	else
	{
		mpf_ui_sub (zee, 1, zee);
		fp_log_m1 (lg, zee, prec);
		mpf_neg (lg, lg);
	}

	/* Add log (2^n) = n log (2) to result. */
	if (0 != nexp)
	{
		fp_log2 (zee, prec);
		if (0 > nexp)
		{
			mpf_mul_ui (zee, zee, -nexp);
			mpf_neg (zee, zee);
		}
		else
		{
			mpf_mul_ui (zee, zee, nexp);
		}
		mpf_add (lg,lg, zee);
	}

	mpf_clear (zee);
}
