/*
 * This file is part of Approximate DNF-Counting Suite.
 * Copyright (c) 2018, Kuldeep S. Meel, Aditya A. Shrotri, Moshe Y. Vardi
 *   
    Approximate DNF-Counting Suite is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Approximate DNF-Counting Suite is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Approximate DNF-Counting Suite.  If not, see <https://www.gnu.org/licenses/>.
    */
    
#include <vector>

#include "../include/DNFDKLNaive.h"
#include "../include/Timers.h"

using std::cout;
using std::endl;

double_t DNFDKLNaive::generateSample(){
	vector<bool> s = DS->randomAssignment();
	if (satisfies(s)){
		return 1;
	}
	return 0;
}

bool DNFDKLNaive::satisfies(vector<bool> s){
	for(int i=1;i<=F.m;i++){
		bool sat = true;
		for(int j=0;j<F.clauses[i].size();j++){
			if(s[abs(F.clauses[i][j])-1]!=(F.clauses[i][j]>0)){
				sat=false;
			}
		}
		if(sat){
			return true;
		}
	}
	return false;
}

int DNFDKLNaive::solve(){
	double startTime = cpuTimeTotal();
	cout<<"starting counting"<<endl;
	AA();
	DS->calculateFraction(count.num, count.den);
	cout<<"finished counting"<<endl;
	double elapsedTime = cpuTimeTotal() - startTime;
	cout<<"DNFDKLNaive took "<<elapsedTime<<" seconds"<<endl;
	cout<<"DNFDKLNaive final result "<<":"<< F.n<<":"<<F.m<<":"<<elapsedTime<<endl;
	return 0;
}
int main(int argc, char* argv[]){
	cout<< "Starting DNFDKLNaive at ";
	print_wall_time();
	cout<<endl;
	
	if (!(argc==4)){
		cout<<"Usage: DNFDKLNaive <eps> <delta> <input_dnf_dimacs_file>"<<endl;
		exit(1);
	}
	double_t eps = strtod(argv[1],NULL);
	double_t delta = strtod(argv[2],NULL);
	std::string dnfFile = std::string(argv[3]);
	
	cout<<"DNFDKLNaive invoked with epsilon="<<eps<<" delta="<<delta<<" dnfFile="<<dnfFile<<endl<<endl;
	DNFParser P;
	if(!P.parse_DNF_dimacs(dnfFile)){
		cout<<"Could not parse dimacs file "<<dnfFile<<endl;
		exit(1);
	}
			
	DNFDKLNaive D(P.getF(),eps,delta);
	int retcode = D.solve();
	
	cout<<endl<<endl<< "DNFDKLNaive ended at ";
	print_wall_time();
	cout<<endl;
	
	return retcode;
	
}	
