/*
 * This file is part of Approximate DNF-Counting Suite.
 * Copyright (c) 2018, Kuldeep S. Meel, Aditya A. Shrotri, Moshe Y. Vardi
 *   
    Approximate DNF-Counting Suite is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Approximate DNF-Counting Suite is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Approximate DNF-Counting Suite.  If not, see <https://www.gnu.org/licenses/>.
    */
    
#include <vector>

#include "../include/DNFDKLM.h"
#include "../include/Timers.h"

using std::cout;
using std::endl;

double_t DNFDKLM::generateSample(){
	vector<bool> s = DS->randomAssignment();
	uint64_t Z1=0;
	for (int i =1;i<=F.m;i++){
		if (satisfies(s,i)){
			Z1++;
			//break;
		}
	}
	//cout<<"Z1:"<<Z1<<" z1/m"<<Z1/(double_t)F.m<<" 1/z1:"<<1/(double_t)Z1<<endl;
	return 1/(double_t)Z1;
}

bool DNFDKLM::satisfies(vector<bool> s, uint64_t clsnum){
	for(int i=0;i<F.clauses[clsnum].size();i++){
		if(s[abs(F.clauses[clsnum][i])-1]!=(F.clauses[clsnum][i]>0)){
			/*cout<<"assignment:"<<endl;
			for (int k=0;k<s.size();k++){
				if (s[k]==0)
					cout<<-(k+1)<<" ";
				else
					cout<<k+1<<" ";
			}
			cout<<endl<<"clause:"<<endl;
			for(int k=0;k<F.clauses[clsnum].size();k++){
				cout<<F.clauses[clsnum][k]<<" ";
			}
			cout<<endl;
			*/
			return false;
		}
	}
	return true;
}

int DNFDKLM::solve(){
	double startTime = cpuTimeTotal();
	cout<<"starting counting"<<endl;
	AA();
	DS->calculateFraction(count.num, count.den);
	cout<<"finished counting"<<endl;
	double elapsedTime = cpuTimeTotal() - startTime;
	cout<<"DNFDKLM took "<<elapsedTime<<" seconds"<<endl;
	cout<<"DNFDKLM final result "<<":"<< F.n<<":"<<F.m<<":"<<elapsedTime<<endl;
	return 0;
}
int main(int argc, char* argv[]){
	cout<< "Starting DNFDKLM at ";
	print_wall_time();
	cout<<endl;
	
	if (!(argc==4)){
		cout<<"Usage: DNFDKLM <eps> <delta> <input_dnf_dimacs_file>"<<endl;
		exit(1);
	}
	double_t eps = strtod(argv[1],NULL);
	double_t delta = strtod(argv[2],NULL);
	std::string dnfFile = std::string(argv[3]);
	
	cout<<"DNFDKLM invoked with epsilon="<<eps<<" delta="<<delta<<" dnfFile="<<dnfFile<<endl<<endl;
	DNFParser P;
	if(!P.parse_DNF_dimacs(dnfFile)){
		cout<<"Could not parse dimacs file "<<dnfFile<<endl;
		exit(1);
	}
			
	DNFDKLM D(P.getF(),eps,delta);
	int retcode = D.solve();
	
	cout<<endl<<endl<< "DNFDKLM ended at ";
	print_wall_time();
	cout<<endl;
	
	return retcode;
}	
