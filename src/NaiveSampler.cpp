/*
 * This file is part of Approximate DNF-Counting Suite.
 * Copyright (c) 2018, Kuldeep S. Meel, Aditya A. Shrotri, Moshe Y. Vardi
 *   
    Approximate DNF-Counting Suite is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Approximate DNF-Counting Suite is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Approximate DNF-Counting Suite.  If not, see <https://www.gnu.org/licenses/>.
    */
    
#include <iostream>
#include <vector>

#include "../include/NaiveSampler.h"

using std::cout;
using std::endl;


vector<bool> NaiveSampler::randomAssignment(){
	vector<bool> result(F.n);
	string asnmtStr = rb.GenerateRandomBits(F.n);
	for (int i=0;i<F.n;i++){
		result[i] = (asnmtStr[i]=='1');
	}
	return(result);
}


void NaiveSampler::calculateFraction(double_t num, uint64_t den){
	double_t lognum = log2(num);
	double_t logden = log2(den);
	cout<<"Final count is 2^"<<F.n+lognum-logden<<endl;
}
