import sys

f1 = open(sys.argv[1],'r')

i = 0
allCubes = set([])
for line in f1:
	if i == 0:
		line1 = line.split()
		nvars = line1[2]
	else:
		a = [int(j) for j in line.split()]
		a.sort()
		del a[0]
		allCubes.add(tuple(a))
	i += 1

f1.close()

f2 = open(sys.argv[2],'w')

f2.write('p cnf '+nvars+' '+str(len(allCubes))+'\n')
for i in allCubes:
	for j in i:
		f2.write(str(j)+' ')
	f2.write('0\n')
f2.close()

print "No. of unique cubes: ",str(len(allCubes))
		
	 
	
