import sys
import os
import random
import math

if len(sys.argv)!=14:
	print 'Usage: python random_dnf_generator.py <numVars_low> <numVars_high> <numVars_step> <cubeDens_low> <cubeDens_high> <cubeDens_step> <cubeWidth_type> <cubeWidth_low> <cubeWidth_high> <cubeWidth_step> <#instances> <outputDir> <monotone>'
	print 'Cubewidth_type = 0 if cubewidth is to be specified as a fraction of numVars. cubewidth_type = 1 if absolute'
	sys.exit(1)

nLow = int(sys.argv[1])
nHigh =  int(sys.argv[2])
nStep = int(sys.argv[3])
mDensityLow = float(sys.argv[4])
mDensityHigh = float(sys.argv[5])
mDensityStep = float(sys.argv[6])
mSizeType = int(sys.argv[7])
mSizeLow = float(sys.argv[8])
mSizeHigh = float(sys.argv[9])
mSizeStep = float(sys.argv[10])
	
instances = int(sys.argv[11])

outputDir = sys.argv[12]
monotone = int(sys.argv[13])

for n in range(nLow,nHigh,nStep):
	m = mDensityLow
	while m < mDensityHigh:
		mSizeLow1 = mSizeLow
		mSizeHigh1 = mSizeHigh
		mSizeStep1 = mSizeStep
		if mSizeType==0:
			mSizeLow1 = n*mSizeLow
			mSizeHigh1 = n*mSizeHigh
			mSizeStep1 = n*mSizeStep
			print n,mSizeLow,mSizeHigh,mSizeStep
		for k in range(int(mSizeLow1),int(mSizeHigh1),int(mSizeStep1)):	
			for i in range(instances):
				opStr = "p cnf "+str(n)+" "+str(int(n*m))+'\n'
				for j in range(int(n*m)):
					sampVars = random.sample(xrange(1,n+1),k)
					for l in sampVars:
						if monotone == 1:
							opStr += str(l)+" "
						else:
							if random.random()>0.5:
								opStr += str(l)+" "
							else:
								opStr += "-"+str(l)+" "
					opStr += "0\n"
				if monotone==1:
					f = open(outputDir+"/randomMonotoneDNF_"+str(n)+"_"+str(int(n*m))+"_"+str(k)+"_"+str(i)+".dnf",'w')
				else:
					f = open(outputDir+"/randomNon-MonotoneDNF_"+str(n)+"_"+str(int(n*m))+"_"+str(k)+"_"+str(i)+".dnf",'w')
				f.write(opStr)
				f.close()
					
		m += mDensityStep

