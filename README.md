[![License: GPL3.0+](https://img.shields.io/badge/LICENSE-GPL3.0%2B-yellow.svg)](https://www.gnu.org/licenses/)

OVERVIEW
========

This is a suite of approximate counting algorithms for DNF formulas. This work is primarily by [Aditya A. Shrotri](https://www.cs.rice.edu/~as128/). Code by [Kuldeep S. Meel](https://www.comp.nus.edu.sg/~meel/) among others (see acknowledgements) is incorporated. 5 Fully Polynomial Randomized Approximation Schemes (FPRASs) are implemented:  

Hashing-based:  
1. DNFApproxMC - based on "[Algorithmic Improvements in Approximate Counting for Probabilistic Inference: From Linear to Logarithmic SAT Calls](https://www.comp.nus.edu.sg/~meel/Papers/ijcai16_counting.pdf)" - Supratik Chakraborty, Meel and Vardi   
2. SymbolicDNFApproxMC - based on the following papers by Meel, Shrotri and Vardi:  
"[Not All FPRASs are Equal: Demystifying FPRASs for DNF-Counting](https://www.cs.rice.edu/~as128/papers/MSV18.pdf)"  
"[On Hashing-Based Approaches to Approximate DNF-Counting](https://www.cs.rice.edu/~as128/papers/MSV17.pdf)"   

Monte Carlo based:  
1. KL Counter - based on "[Monte-Carlo algorithms for enumeration and reliability problems](https://ieeexplore.ieee.org/abstract/document/4568061)" - Karp and Luby  and ["An Optimal Algorithm for Monte Carlo Estimattion"](www.icsi.berkeley.edu/~luby/PAPERS/optspeedup.ps) - Dagum, Karp, Luby, Ross 
2. KLM Counter - based on "[Monte Carlo Approximation Algorithms for Enumeration Problems](https://www.math.cmu.edu/~af1p/Teaching/MCC17/Papers/KLM.pdf)" - Karp, Luby and Madras  
3. Vazirani Counter - from "[Approximation Algorithms (book)](https://dl.acm.org/citation.cfm?id=500776)" - Vazirani  
 
We also implement the naive Monte Carlo counter.  

LICENSE:
========

GPL-3.0-or-later

See COPYING

REQUIREMENTS:
=============

GCC version 4.8 or higher  
[M4RI](https://bitbucket.org/malb/m4ri)  
[GNU Bignum 6.1.2](https://gmplib.org/)   

For best performance, ensure M4RI is configured to use SSE2 instructions  

INSTALLATION
==============

Currently there 2 Makefiles (Makefile and Makefile_custom) for building. We will migrate to CMake soon. If M4RI and Bignum are configured to be on the build path, then running `make` will suffice.  
Else, set the environment variables $M4RI_INCLUDE, $M4RI_LIBRARIES, $GMP_LIBS and $GMP_INCLUDE to the appropriate installation directories. For example,  

```
export M4RI_INCLUDE="/path/to/dir/m4ri/m4ri/"
export M4RI_LIBRARIES="/path/to/dir/m4ri/.libs/"
export GMP_LIBS="/path/to/dir/gmp-6.1.2/"
export GMP_INCLUDE="/path/to/dir/gmp-6.1.2/"
```

Then run   

```
make -f Makefile_custom  
```

INPUT FORMAT
============

We follow the same DIMACS format that is commonly used for CNF files. The algorithms interpret the files as DNF formulas instead of CNF. For example, the DNF formula
```
(!a AND b) OR (!b AND c) 
```
will be encoded as

```
p dnf 3 2
-1 2 0
-2 3 0
```

Even if the header line is `p cnf 3 2`, the file will still be interpreted as a DNF formula (since this is a suite of DNF counting algorithms)
 
USAGE
=====

1. Algorithm    - DNFApproxMC  
   Binary file  - DNFCUSP  
   Syntax       - `./DNFCUSP <eps> <delta> <search_mode> <timeout> <path_to_log_file> <path_to_input_dimacs_file>`  
   Example      - `./DNFCUSP 0.8 0.36 0 0 /path/to/dir/logfile.log /path/to/dir/test.dnf`  
   Arguments    - eps and delta are the input tolerance and confidence between 0 and 1. timeout has no effect - use bash command 'timeout' instead. search_mode (0,1,2) determines whether to use linear search (0), binary search (1), or reverse search (2).   

2. Algorithm    - SymbolicDNFApproxMC  
   Binary file  - DNFKLMCUSP  
   Syntax       - `./DNFKLMCUSP <eps> <delta> <search_mode> <path_to_log_file> <path_to_input_dimacs_file>`  
   Example      - `./DNFKLMCUSP 0.8 0.36 2 0 /path/to/dir/logfile.log /path/to/dir/test.dnf`  
   Arguments    - eps and delta are the input tolerance and confidence between 0 and 1. timeout has no effect - use bash command 'timeout' instead. search_mode (0,1,2) determines whether to use linear search (0), binary search (1), or reverse search (2).  
   
3. Algorithm    - KL Counter  
   Binary file  - DNFDKL  
   Syntax       - `./DNFDKL <eps> <delta> <path_to_input_dimacs_file>`  
   Example      - `./DNFDKL 0.8 0.36 /path/to/dir/test.dnf`  
   Arguments    - eps and delta are the input tolerance and confidence between 0 and 1  
   
4. Algorithm    - KLM Counter  
   Binary file  - DNFKLM  
   Syntax       - `./DNFKLM <eps> <delta> <path_to_input_dimacs_file>`  
   Example      - `./DNFKLM 0.8 0.36 /path/to/dir/test.dnf`  
   Arguments    - eps and delta are the input tolerance and confidence between 0 and 1  

5. Algorithm    - Vazirani Counter  
   Binary file  - DNFDKLM  
   Syntax       - `./DNFDKLM <eps> <delta> <path_to_input_dimacs_file>`  
   Example      - `./DNFDKLM 0.8 0.36 /path/to/dir/test.dnf`  
   Arguments    - eps and delta are the input tolerance and confidence between 0 and 1  

6. Algorithm    - Naive Counter  
   Binary file  - DNFDKLNaive  
   Syntax       - `./DNFDKLNaive <eps> <delta> <path_to_input_dimacs_file>`  
   Example      - `./DNFDKLNaive 0.8 0.36 /path/to/dir/test.dnf`  
   Arguments    - eps and delta are the input tolerance and confidence between 0 and 1  
   

RANDOM GENERATION
=================

The script random_dnf_generator.py in the scripts directory can be used to generate random dnf formulas over ranges of the parameters #variables, cube-density, cubewidths, monotone/non-monotone, and number of instances of each parameter setting to generate. For detailed syntax do `python random_dnf_generator.py` from the scripts directory. Sample commands are given in random_generator_commands.txt  

Note: The script generates files with header lines as `p cnf`. This is to ensure compatibility with CNF counting algorithms which require `p cnf` to be in the header. As mentioned above, our algorithms work equally well with either `p cnf` or `p dnf`, and interpret the file to be a DNF formula in either case.


ACKNOWLEDGEMENTS:
=================

Kuldeep S. Meel's implementation of an earlier version of [ApproxMC](https://github.com/meelgroup/ApproxMC) was adapted for the hashing-based approaches. Thanks are due to the contributors therein. The [MayBMS](http://maybms.sourceforge.net/) development group's implementation of the Optimal Stopping Algorithm of Dagum, Karp, Luby and Ross was adapted for use with the Monte Carlo based approaches. Authors are grateful to [Mate Soos](msoos.org) for providing fixes to an earlier version.

CITING US:
==========

Please cite the paper "[Not All FPRASs are Equal: Demystifying FPRASs for DNF-Counting](https://link.springer.com/article/10.1007/s10601-018-9301-x)", with [BibTex](https://scholar.googleusercontent.com/scholar.bib?q=info:ZPwvvGAW9CIJ:scholar.google.com/&output=citation&scisig=AAGBfm0AAAAAXFIXJ5ljRFogAFNRwAnlDKfAARsZGcuQ&scisf=4&ct=citation&cd=-1&hl=en)
