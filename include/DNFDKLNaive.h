/*
 * This file is part of Approximate DNF-Counting Suite.
 * Copyright (c) 2018, Kuldeep S. Meel, Aditya A. Shrotri, Moshe Y. Vardi
 *   
    Approximate DNF-Counting Suite is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Approximate DNF-Counting Suite is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Approximate DNF-Counting Suite.  If not, see <https://www.gnu.org/licenses/>.
    */
    
#ifndef DNFDKLNaive_H_
#define DNFDKLNaive_H_

#include "DKL.h"
#include "NaiveSampler.h"
#include "DNFParser.h"
#include "DNFFormula.h"


class DNFDKLNaive: public DKL{
	public:
		DNFDKLNaive(DNFFormula F_, double_t eps_, double_t delta_):
		DKL(eps_, delta_), F(F_){
			DS = new NaiveSampler(F);
		}
		int solve() override;
					
	protected:
		double_t generateSample() override;
		bool satisfies(vector<bool> s);
		
		NaiveSampler *DS;
		DNFFormula F;
};

#endif
