/*
 * This file is part of Approximate DNF-Counting Suite.
 * Copyright (c) 2018, Kuldeep S. Meel, Aditya A. Shrotri, Moshe Y. Vardi
 *   
    Approximate DNF-Counting Suite is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Approximate DNF-Counting Suite is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Approximate DNF-Counting Suite.  If not, see <https://www.gnu.org/licenses/>.
    */
    
#ifndef WDNFDKL_H_
#define WDNFDKL_H_

#include "DKL.h"
#include "WDNFSampler.h"
#include "DNFParser.h"
#include "DNFFormula.h"


class WDNFDKL: public DKL{
	public:
		WDNFDKL(DNFFormula F_, vector<uint32_t> weights_, vector<double_t> clauseWeights_, uint32_t bitLen_, double_t eps_, double_t delta_):
		DKL(eps_, delta_), F(F_){
			DS = new WDNFSampler(F, weights_, bitLen_, clauseWeights_);
		}
		int solve() override;
					
	protected:
		double_t generateSample() override;
		bool satisfies(vector<bool> s, uint64_t clsnum);
		
		WDNFSampler *DS;
		DNFFormula F;
};

#endif
