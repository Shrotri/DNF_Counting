/*
 * This file is part of Approximate DNF-Counting Suite.
 * Copyright (c) 2018, Kuldeep S. Meel, Aditya A. Shrotri, Moshe Y. Vardi
 *   
    Approximate DNF-Counting Suite is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Approximate DNF-Counting Suite is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Approximate DNF-Counting Suite.  If not, see <https://www.gnu.org/licenses/>.
    */
    
#ifndef NETWORK_H_
#define NETWORK_H_

#include <fstream>
#include <sstream>
#include <iostream>
#include <cassert>

#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/graph_traits.hpp>
#include <boost/graph/breadth_first_search.hpp>

#include "RandomBits.h"

using std::ifstream;
using std::cout;
using std::endl;
using std::stoi;
using std::stringstream;
using std::pair;
using std::vector;

class Network{
		
		public:
			Network(string file){
					ifstream inputFile;
					inputFile.open(file);
					
					if(!inputFile.is_open()){
						cout<<"Error opening file "<<file<<endl;
						exit(1);
					}
					
					vector<pair<uint64_t,uint64_t>> edgeVec;
					n=0,m=0; //#vertices #edges
					string line;
					bool flag=false;
					int i = 0;
					vector<int64_t> a(0);
					while(getline(inputFile,line)){
						
						if (i==0){
							boost::add_vertex(g);
							int64_t edgePresent;
							stringstream sline(line);
							while (!sline.eof()){
								sline>>edgePresent;
								if(edgePresent){
									edgeVec.push_back(pair<uint64_t,uint64_t>(0,n));
									m++;
								}
								n++;
							}
							i++;
							continue;
						}
						
						int64_t edgePresent;
						stringstream sline(line);
						for(int j=0;j<n;j++){
							sline>>edgePresent;
							if(edgePresent){
								edgeVec.push_back(pair<uint64_t,uint64_t>(i,j));
								m++;
							}
						}
						i++;
					}
					if (i<n){
						cout<<"Number of vertices: "<<n<<" Number of lines: "<<i<<endl;
						exit(1);
					}
					inputFile.close();
					cout<<"number of vertices is "<<boost::num_vertices(g)<<endl;
					cout<<"number of edges is "<<boost::num_edges(g)<<endl;
					//printGraph();					
			}
			Network(const Network& G_){
				this->g=G_.g;
				this->n=G_.n;
				this->m=G_.m;
			}
			
			bool failEdges(RandomBits rb);
			bool areConnected(uint64_t s, uint64_t t);
			bool printGraph();
		private:
			typedef boost::property<boost::edge_weight_t, double_t> EdgeWeightProperty;
			typedef boost::adjacency_list<boost::listS, boost::vecS,boost::undirectedS,boost::no_property,EdgeWeightProperty> UndirectedGraph;
			UndirectedGraph g;
			uint64_t n,m;
};
#endif

