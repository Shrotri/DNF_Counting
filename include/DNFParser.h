/*
 * This file is part of Approximate DNF-Counting Suite.
 * Copyright (c) 2018, Kuldeep S. Meel, Aditya A. Shrotri, Moshe Y. Vardi
 *   
    Approximate DNF-Counting Suite is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Approximate DNF-Counting Suite is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Approximate DNF-Counting Suite.  If not, see <https://www.gnu.org/licenses/>.
    */
#ifndef DNFPARSER_H_
#define DNFPARSER_H_

#include <vector>
#include <string>
#include <cmath>

#include "DNFFormula.h"

using std::string;
using std::vector;

class DNFParser{
public:
	bool parse_DNF_dimacs(string file);
	bool parse_W2U_DNF_dimacs(string file);
	DNFFormula getF();
	uint32_t getBitLen();
	vector<uint32_t> getWeights();
	vector<double_t> getClauseWeights();
	double_t getMaxClauseWt();
	double_t getTotClauseWt();
private:
	DNFFormula *F;
	vector<uint32_t> weights;
	uint32_t bitLen;
	vector<double_t> clauseWeights;
	double_t totClauseWt = 0;
	double_t maxClauseWt = 0;
};

#endif
