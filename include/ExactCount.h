/*
 * This file is part of Approximate DNF-Counting Suite.
 * Copyright (c) 2018, Kuldeep S. Meel, Aditya A. Shrotri, Moshe Y. Vardi
 *   
    Approximate DNF-Counting Suite is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Approximate DNF-Counting Suite is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Approximate DNF-Counting Suite.  If not, see <https://www.gnu.org/licenses/>.
    */
    
#ifndef EXACTCOUNT_H_
#define EXACTCOUNT_H_

#include <iostream>
#include <vector>
#include <cmath>

#include "DNFParser.h"
#include "DNFFormula.h"

using std::vector;
class ExactCount{
	public:
		ExactCount(DNFFormula F_, uint32_t bitLen_, vector<uint32_t> weights_, uint32_t timeOut_):
		F(F_), bitLen(bitLen_), weights(weights_), timeOut(timeOut_){mode = 1;}
		ExactCount(DNFFormula F_, uint32_t timeOut_):
		F(F_), timeOut(timeOut_){mode = 0;}	
		int solve();
		bool nextAsmt(vector<bool>&);
		bool satisfies(vector<bool> s, uint64_t clsnum);
		double_t weight(vector<bool> asmt);
	protected:
		vector<vector<bool>> fails;
		DNFFormula F;
		uint32_t bitLen;
		vector<uint32_t> weights;
		uint32_t timeOut;
		double_t prob;
		uint64_t count;
		int mode;
};
#endif
