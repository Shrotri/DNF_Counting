/*
 * This file is part of Approximate DNF-Counting Suite.
 * Copyright (c) 2018, Kuldeep S. Meel, Aditya A. Shrotri, Moshe Y. Vardi
 *   
    Approximate DNF-Counting Suite is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Approximate DNF-Counting Suite is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Approximate DNF-Counting Suite.  If not, see <https://www.gnu.org/licenses/>.
    */
    
#ifndef NWDKL_H_
#define NWDKL_H_

#include "DKL.h"
#include "Network.h"
#include "RandomBits.h"

class NWDKL: public DKL{
	public:
		NWDKL(Network *G_, double_t eps_, double_t delta_, uint64_t source_, uint64_t target_):
		DKL(eps_, delta_), G(G_), source(source_), target(target_){rb.SeedEngine();}
		int solve() override;
					
	protected:
		uint64_t source,target;
		double_t generateSample() override;
		Network *G;
		RandomBits rb;
};

#endif
