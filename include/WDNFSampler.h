/*
 * This file is part of Approximate DNF-Counting Suite.
 * Copyright (c) 2018, Kuldeep S. Meel, Aditya A. Shrotri, Moshe Y. Vardi
 *   
    Approximate DNF-Counting Suite is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Approximate DNF-Counting Suite is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Approximate DNF-Counting Suite.  If not, see <https://www.gnu.org/licenses/>.
    */
    
#ifndef WDNFSAMPLER_H_
#define WDNFSAMPLER_H_

#include <gmp.h>
#include <gmpxx.h>
#include <ctime>
#include <vector>
#include <cmath>

#include "DNFFormula.h"
#include "RandomBits.h"

using std::cout;
using std::endl;

class WDNFSampler{
	public:
		WDNFSampler(DNFFormula F_, vector<uint32_t> weights_, uint32_t bitLen_, vector<double_t> clauseWeights_):
		F(F_), weights(weights_), bitLen(bitLen_), clauseWeights(clauseWeights_){
			sampling_array.resize(F.m+1);
			for (int i = 1; i<= F.m;i++){
				sampling_array[i] = clauseWeights[i] + sampling_array[i-1];
			}
			U = sampling_array[F.m];
			cout<<"U: "<<U<<endl;
			rb.SeedEngine();
			rb.SeedEngine2();
		}
		vector<bool> randomAssignment();
		void calculateFraction(uint64_t num, uint64_t den);
		
		uint32_t randClsNum;
	protected:
		
		DNFFormula F;
		vector<uint32_t> weights; 
		uint32_t bitLen; 
		vector<double_t> clauseWeights;
		vector<double_t> sampling_array;
		double_t U;
		RandomBits rb;
};
#endif
