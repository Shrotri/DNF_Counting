/*
 * This file is part of Approximate DNF-Counting Suite.
 * Copyright (c) 2018, Kuldeep S. Meel, Aditya A. Shrotri, Moshe Y. Vardi
 *   
    Approximate DNF-Counting Suite is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Approximate DNF-Counting Suite is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Approximate DNF-Counting Suite.  If not, see <https://www.gnu.org/licenses/>.
    */
    
#ifndef DKL_H_
#define DKL_H_

#include <cmath>
#include <iostream>

struct SampleCount{
	double_t num;
	uint64_t den;
};

class DKL{
	public:
		DKL(double_t eps_, double_t delta_, int _seed = -1):
		epsilon(eps_), delta(delta_){}
		virtual int solve() = 0;		
	
	protected:
		int AA();
			
		virtual double_t generateSample() = 0;
		
		double_t epsilon, delta;
		SampleCount count;
};

#endif
