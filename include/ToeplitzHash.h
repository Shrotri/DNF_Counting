/*
 * This file is part of Approximate DNF-Counting Suite.
 * Copyright (c) 2018, Kuldeep S. Meel, Aditya A. Shrotri, Moshe Y. Vardi
 *   
    Approximate DNF-Counting Suite is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Approximate DNF-Counting Suite is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Approximate DNF-Counting Suite.  If not, see <https://www.gnu.org/licenses/>.
    */
    
#ifndef TOEPLITZHASH_H_
#define TOEPLITZHASH_H_

#include <m4ri/m4ri.h>
#include <vector>
#include <cassert>
#include <iostream>

#include "RandomBits.h"
#include "Hash.h"


using std::vector;

class ToeplitzHash : public Hash{
	public:
		ToeplitzHash(uint32_t nVars, uint64_t startIteration_) : Hash(nVars) {
			rb.SeedEngine();
			startIteration = startIteration_;
			maxHashCount = numVars;
			rectHash = mzd_init(numVars,numVars+1);
			echHash = mzd_init(1,1); // dummy init
			enumHash = mzd_init(numVars,numVars+1);
			constraints=vector<int64_t>();
			//numFreeVars = nVars;
			
			
			//std::cout<<"Created assumps "<<rectHash<<std::endl;
			//mzd_print(rectHash);
			//std::cout<<"Printed rectHash in constructor"<<std::endl;
			
		}
		~ToeplitzHash(){
			if (mzd_owns_blocks(rectHash)){
				mzd_free(rectHash);
			}
			if (mzd_owns_blocks(echHash)){
				mzd_free(echHash);
			}
			if (mzd_owns_blocks(enumHash)){
				mzd_free(enumHash);
			}
			
		}
		
		vector<bool> enumerateNextSoln(bool& last);
		//vector<bool> enumerateNextGraySoln(bool& last);
		bool initHash(uint32_t numStartHashes) override;
		bool AddHash(uint32_t num_xor_cls) override;
		bool SetHash(uint32_t num_xor_cls) override;
		bool SubHash(uint32_t num_xor_cls) override;
		bool AddFlippedHash() override;
		bool SubFlippedHash() override;
		uint32_t simplify();
		void AddConstraints(string con);
		bool RemoveAllConstraints();
		//bool clear(bool init) override;
		
	private:
		
		bool nextFreeVarAssnmnt();
		bool testResult(vector<bool> s);
		
		bool countFlipped = false; //used for SubHash
		uint64_t startIteration;
		//uint64_t endIteration;
		uint64_t maxHashCount;
		uint32_t numFreeVars;
		RandomBits rb;
		string firstRow = "", firstCol = "", lastCol="";
		
		mzd_t* rectHash;
		
		mzd_t* echHash;
		mzd_t* enumHash;
		
		uint32_t numCons=0;
		uint32_t echRank;
		bool hasConstraints;
		vector<int64_t> constraints;
		
};

#endif
