/*
 * This file is part of Approximate DNF-Counting Suite.
 * Copyright (c) 2018, Kuldeep S. Meel, Aditya A. Shrotri, Moshe Y. Vardi
 *   
    Approximate DNF-Counting Suite is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Approximate DNF-Counting Suite is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Approximate DNF-Counting Suite.  If not, see <https://www.gnu.org/licenses/>.
    */
    
#ifndef ROWECHELONHASH_H_
#define ROWECHELONHASH_H_
//new
#include <m4ri/m4ri.h>
#include <vector>
#include <cassert>
#include <iostream>
#include <unordered_map>
#include <set>

#include "RandomBits.h"
#include "Hash.h"
#include "Gray.h"
#include "HashSolution.h"

using std::vector;

class RowEchelonHash : public Hash{
	public:
		RowEchelonHash(uint32_t nVars, uint64_t maxHashCount, uint64_t maxConsSize_, int seed) : Hash(nVars), g(Gray(numVars)) {
			assert(maxHashCount >= nVars);
			this->maxHashCount = maxHashCount;
			maxConsSize = maxConsSize_;
            if (seed == -1) {
                rb.SeedEngine();
            } else {
                rb.SeedFixed(seed);
            }
			startIteration = maxHashCount - numVars;
			//endIteration = endIteration_;
			//currHashCount = startIteration;
			rectHash = mzd_init(maxHashCount,numVars+1);
			diagHash = mzd_init(numVars, numVars+1);
			
			freeVarAssnmnt = mzd_init(1,nVars+1);
			//numFreeVars = nVars;
			
			#ifdef DBUG
			testHash = mzd_init(startIteration,maxHashCount+1);               //UNCOMMENT WHILE TESTING
			testEnumHash = mzd_init(2*maxHashCount+maxConsSize,maxHashCount+1);                 //UNCOMMENT WHILE TESTING
			#endif
			extractHash = mzd_init(numVars+1, maxHashCount);
			extractAssmt = mzd_init(1, maxHashCount);
			constraints=vector<int64_t>();
			grayFreeVarLocs = vector<int32_t>(numVars);
			for(int i = 0; i<numVars;i++){
				grayFreeVarLocs[i] = i;
			}
			grayBoundVarLocs = vector<uint32_t>(numVars);
			subbed = false;
			flipped = false;
			//std::cout<<"Created assumps "<<rectHash<<std::endl;
			//mzd_print(rectHash);
			//std::cout<<"Printed rectHash in constructor"<<std::endl;
			constraintHash = mzd_init(maxHashCount+maxConsSize,numVars+1);
			constraintNumCols = numVars+1;
		}
		bool subbed = false;
		bool flipped = false;
		bool inited = false;
		HashSolution* enumerateNextGraySoln(bool& last);
		void initHash(uint32_t numStartHashes) override;
		bool AddHash(uint32_t num_xor_cls) override;
		bool SetHash(uint32_t num_xor_cls) override;
		bool SubHash(uint32_t num_xor_cls) override;
		bool AddFlippedHash() override;
		bool SubFlippedHash() override;
		//bool clear(bool init) override;
		uint32_t simplify(uint32_t maxsols);
		void AddConstraints(string con);
		void AddConstraint(uint32_t consCol, uint32_t origCol);
		uint64_t RemoveAllConstraints();
		bool checkAsnmt(const HashSolution &s);
		
	private:
		uint64_t prevHashCount = 0;
		bool nextFreeVarAssnmnt();
		#ifdef DBUG
		bool testResult(const HashSolution &s1, bool addCons);               //UNCOMMENT WHILE TESTING
		#endif
		
		bool countFlipped = false; //used for SubHash
		uint64_t startIteration;
		//uint64_t endIteration;
		uint64_t maxHashCount;
		uint32_t numFreeVars;
		RandomBits rb;
		mzd_t* rectHash;
		mzd_t* diagHash;
		
		mzd_t* constraintHash;
		mzd_t* constraintEchHash;
		
		mzd_t* freeVarAssnmnt;
		
		#ifdef DBUG
		mzd_t* testHash;                 //UNCOMMENT WHILE TESTING
		mzd_t* testEnumHash;               //UNCOMMENT WHILE TESTING
		#endif
		mzd_t* extractHash;
		mzd_t* extractAssmt;
		Gray g;
		
		vector<vector<uint64_t>> extractRows;
		
		vector<int32_t> grayFreeVarLocs;
		vector<uint32_t> grayBoundVarLocs;
		
		uint64_t maxConsSize=0;
		uint32_t numCons=0;
		uint32_t consRank=0;
		bool hasConstraints;
		vector<int64_t> constraints;
		std::unordered_map<uint64_t,vector<uint64_t>> constraintRows;
		std::set<uint64_t> constraintSkipRows;  
		uint64_t constraintNumCols;
};

#endif
