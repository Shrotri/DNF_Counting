/*
 * This file is part of Approximate DNF-Counting Suite.
 * Copyright (c) 2018, Kuldeep S. Meel, Aditya A. Shrotri, Moshe Y. Vardi
 *   
    Approximate DNF-Counting Suite is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Approximate DNF-Counting Suite is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Approximate DNF-Counting Suite.  If not, see <https://www.gnu.org/licenses/>.
    */
    
#ifndef DNFKLMCUSP_H_
#define DNFKLMCUSP_H_

#include <iostream>

#include "DNFFormula.h"
#include "CUSP.h"
#include "RowEchelonHash.h"
#include "RandomBits.h"
#include "HashSolution.h"
#include "KLSampler.h"

class DNFKLMCUSP: public CUSP{

	public:
		DNFKLMCUSP(DNFFormula F_, uint32_t pivotApproxMC_, 	uint32_t tApproxMC_, uint32_t searchMode_, uint32_t loopTimeout_, std::string cuspLogFile_, int seed)
		: CUSP(pivotApproxMC_, tApproxMC_, searchMode_, loopTimeout_, cuspLogFile_), F(F_) {
			logm = (uint32_t)ceil(log2(F_.m));
			logpivot = (uint32_t)ceil(log2(pivotApproxMC_));
			numClsBits = 0;
			
			countTypeFixed = false; // false means count using variable number of clause bits. true means use fixed clause bits but discard assignments 
			uniform = true;
			if (F.minClauseSize == F.maxClauseSize){
				numClsBits = logm;
				uniform = true;
			}
			else{
				if (countTypeFixed){
					numClsBits = logm;
					uniform = true;
				}
				else{
					DS = new KLSampler(F);
					numClsBits = (uint32_t) ceil(DS->getLogU());
					uniform = false;
				}
			}
			assert(numClsBits>0);
			numHashVars = F.n-F.minClauseSize+numClsBits;
			assumps = new RowEchelonHash(logm+logpivot,numHashVars,F.maxClauseSize, seed);
			startIteration = numHashVars - logm - logpivot;
			endIteration = numHashVars - logpivot;
			std::cout<<"StartIteration in constructor:"<<startIteration<<std::endl;
			std::cout<<"endIteration in constructor:"<<endIteration<<std::endl;
			std::cout<<"numClsBits in constructor:"<<numClsBits<<std::endl;
			std::cout<<"logm in constructor:"<<logm<<std::endl;
			std::cout<<"minWidth maxWidth in constructor:"<<F.minClauseSize<<" "<<F.maxClauseSize<<std::endl;
			rb.SeedEngine();
		}
			
		int solve(int seed) override;
	
	protected:
		double_t BoundedSATCount(double_t maxSolutions, bool upperBoundCheck) override;
		
		virtual int getSigma(vector<bool> hashSoln, vector<bool>& sigma);
		bool satisfies(vector<bool> s, uint64_t clsnum);
		
		DNFFormula F;
		uint32_t logpivot, logm, numClsBits;
		
		KLSampler *DS;
		bool uniform;
		
		bool countTypeFixed;
		
		std::uniform_int_distribution<uint64_t> uid {1,F.m};
		RandomBits rb;
		
};

#endif
