all: DNFParser.o KLSampler.o Gray.o CUSP.o RandomBits.o DNFDKL.o DNFDKLNaive.o DNFDKLM.o DNFKLM.o RowEchelonHash.o DNFCUSP.o DNFCUSP_main.o DNFKLMCUSP.o DNFKLMCUSP_main.o ExactCount.o
	g++ -std=c++11 -O3 -ggdb -Wall RandomBits.o DNFCUSP.o CUSP.o Gray.o RowEchelonHash.o DNFParser.o DNFCUSP_main.o -o bin/DNFCUSP -lm4ri -lgmp
	g++ -std=c++11 -O3 -ggdb -Wall RandomBits.o Gray.o DNFKLMCUSP.o CUSP.o KLSampler.o RowEchelonHash.o DNFParser.o DNFKLMCUSP_main.o -o bin/DNFKLMCUSP -lm4ri -lgmp
	g++ -std=c++11 -O3 -ggdb -Wall DNFParser.o KLSampler.o RandomBits.o DNFKLM.o -o bin/DNFKLM -lgmp
	g++ -std=c++11 -O3 -ggdb -Wall DNFParser.o RandomBits.o KLSampler.o DKL.o DNFDKL.o -o bin/DNFDKL -lgmp
	g++ -std=c++11 -O3 -ggdb -Wall DNFParser.o RandomBits.o NaiveSampler.o DKL.o DNFDKLNaive.o -o bin/DNFDKLNaive
	g++ -std=c++11 -O3 -ggdb -Wall DNFParser.o RandomBits.o KLSampler.o DKL.o DNFDKLM.o -o bin/DNFDKLM -lgmp
	g++ -std=c++11 -O3 -ggdb -Wall DNFParser.o ExactCount.o -o bin/ExactCount
		
Gray.o: src/Gray.cpp include/Gray.h
	g++ -std=c++11 -O3 -ggdb -Wall -c src/Gray.cpp

DNFParser.o: src/DNFParser.cpp include/DNFParser.h include/DNFFormula.h
	g++ -std=c++11 -O3 -ggdb -Wall -c src/DNFParser.cpp


RandomBits.o: src/RandomBits.cpp include/RandomBits.h
	g++ -std=c++11 -O3 -ggdb -Wall -c src/RandomBits.cpp

KLSampler.o: include/Sampler.h src/KLSampler.cpp include/KLSampler.h include/DNFFormula.h RandomBits.o
	g++ -std=c++11 -O3 -ggdb -Wall -c src/KLSampler.cpp

NaiveSampler.o: include/Sampler.h src/NaiveSampler.cpp include/NaiveSampler.h include/DNFFormula.h RandomBits.o
	g++ -std=c++11 -O3 -ggdb -Wall -c src/NaiveSampler.cpp
	
DKL.o: src/DKL.cpp include/DKL.h
	g++ -std=c++11 -O3 -ggdb -Wall -c src/DKL.cpp
	
DNFDKL.o: src/DNFDKL.cpp include/DNFDKL.h DKL.o KLSampler.o DNFParser.o include/DNFFormula.h
	g++ -std=c++11 -O3 -ggdb -Wall -c src/DNFDKL.cpp

DNFDKLNaive.o: src/DNFDKLNaive.cpp include/DNFDKLNaive.h DKL.o NaiveSampler.o DNFParser.o include/DNFFormula.h
	g++ -std=c++11 -O3 -ggdb -Wall -c src/DNFDKLNaive.cpp

DNFDKLM.o: src/DNFDKLM.cpp include/DNFDKLM.h DKL.o KLSampler.o DNFParser.o include/DNFFormula.h
	g++ -std=c++11 -O3 -ggdb -Wall -c src/DNFDKLM.cpp

DNFKLM.o: src/DNFKLM.cpp include/DNFKLM.h KLSampler.o DNFParser.o RandomBits.o include/DNFFormula.h
	g++ -std=c++11 -O3 -ggdb -Wall -c src/DNFKLM.cpp
	
CUSP.o: src/CUSP.cpp include/CUSP.h include/Timers.h include/Hash.h
	g++ -std=c++11 -O3 -ggdb -Wall -c src/CUSP.cpp

RowEchelonHash.o: src/RowEchelonHash.cpp include/RowEchelonHash.h include/Hash.h include/Gray.h include/HashSolution.h
	g++ -std=c++11 -O3 -ggdb -Wall -c src/RowEchelonHash.cpp

DNFCUSP.o: src/DNFCUSP.cpp include/DNFCUSP.h CUSP.o RowEchelonHash.o include/Timers.h include/HashSolution.h include/DNFUpperBound.h
	g++ -std=c++11 -O3 -ggdb -Wall -c src/DNFCUSP.cpp

DNFCUSP_main.o: src/DNFCUSP_main.cpp include/DNFCUSP.h CUSP.o RowEchelonHash.o include/Timers.h
	g++ -std=c++11 -O3 -ggdb -Wall -c src/DNFCUSP_main.cpp

DNFKLMCUSP.o: src/DNFKLMCUSP.cpp include/DNFKLMCUSP.h CUSP.o RowEchelonHash.o KLSampler.o include/Timers.h
	g++ -std=c++11 -O3 -ggdb -Wall -c src/DNFKLMCUSP.cpp
	
DNFKLMCUSP_main.o: src/DNFKLMCUSP_main.cpp include/DNFKLMCUSP.h CUSP.o RowEchelonHash.o include/Timers.h
	g++ -std=c++11 -O3 -ggdb -Wall -c src/DNFKLMCUSP_main.cpp

ExactCount.o: include/ExactCount.h include/DNFParser.h src/ExactCount.cpp
	g++ -std=c++11 -O3 -ggdb -Wall -c src/ExactCount.cpp

clean:
	rm *.o
	rm bin/DNFCUSP bin/DNFDKL  bin/DNFDKLM  bin/DNFDKLNaive  bin/DNFKLM  bin/DNFKLMCUSP  bin/ExactCount
